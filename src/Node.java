import java.util.HashMap;

public class Node {

	public int ID;
	public int K;
	public int Npa;
	public HashMap<String, Double> maps[];
	public Node parents[];

	@SuppressWarnings("unchecked")
	public Node(int _ID, int _K, int _Npa) {
		ID = _ID;
		K = _K;
		maps = (HashMap<String, Double>[]) new HashMap[K];
		for (int i = 0; i < K; ++i) {
			maps[i] = new HashMap<String, Double>();
		}
		Npa = _Npa;
		parents = new Node[Npa];
	}

	/**
	 * Returns the number of combinations of all possible values of the parents.
	 */
	public int combinations() {
		int retval = 1;
		for (int i = 0; i < Npa; ++i) {
			retval *= parents[i].K;
		}
		return retval;
	}

	public Double probability(int of, String condition) {
		return maps[of].get(condition);
	}
}
