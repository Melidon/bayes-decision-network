import java.util.HashMap;

public class DecisionNet {

	public int Nv;
	public Node nodes[];
	public int Ne;
	public HashMap<Node, Integer> E;
	public Node targetVariable;
	public int Nd;
	public double[][] usefulness;

	public DecisionNet(int _Np) {
		Nv = _Np;
		nodes = new Node[Nv];
		E = new HashMap<Node, Integer>();
	}

	public double[] enumerationAsk() {
		double distribution[] = new double[targetVariable.K];
		for (int i = 0; i < targetVariable.K; ++i) {
			E.put(targetVariable, i);
			distribution[i] = enumerateAll(0);
			E.remove(targetVariable);
		}
		return normalize(distribution);
	}

	private double enumerateAll(int first) {
		if (first == nodes.length) {
			return 1.0;
		}
		Node Y = nodes[first];
		Integer y = E.get(Y);
		if (y != null) {
			return Y.probability(y, getCombination(Y)) * enumerateAll(first + 1);
		} else {
			double sum = 0.0;
			for (y = 0; y < Y.K; ++y) {
				E.put(Y, y);
				sum += Y.probability(y, getCombination(Y)) * enumerateAll(first + 1);
				E.remove(Y);
			}
			return sum;
		}
	}

	private static double[] normalize(double[] distribution) {
		double sum = 0;
		for (int i = 0; i < distribution.length; ++i) {
			sum += distribution[i];
		}
		for (int i = 0; i < distribution.length; ++i) {
			distribution[i] /= sum;
		}
		return distribution;
	}

	public String getCombination(Node node) {
		String combination = "";
		for (int i = 0; i < node.parents.length; ++i) {
			combination += E.get(node.parents[i]); // It shall never return null.
			if (i < node.parents.length - 1) {
				combination += ",";
			}
		}
		return combination;
	}
}
