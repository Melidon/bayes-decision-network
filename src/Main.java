import java.util.Scanner;

public class Main {

	public static DecisionNet readInput() {
		Scanner sc = new Scanner(System.in);
		DecisionNet net = new DecisionNet(sc.nextInt());
		for (int i = 0; i < net.Nv; ++i) {
			net.nodes[i] = new Node(i, sc.nextInt(), sc.nextInt());

			for (int j = 0; j < net.nodes[i].Npa; ++j) {
				net.nodes[i].parents[j] = net.nodes[sc.nextInt()];
			}
			for (int j = 0; j < net.nodes[i].combinations(); ++j) {
				String combination = "";
				if (net.nodes[i].combinations() > 1) {
					sc.useDelimiter("[\\s:]");
					combination = sc.next();
				}
				for (int k = 0; k < net.nodes[i].K; ++k) {
					sc.useDelimiter("[\\s,:]");
					net.nodes[i].maps[k].put(combination, sc.nextDouble());
				}
			}
		}
		net.Ne = sc.nextInt();
		for (int i = 0; i < net.Ne; ++i) {
			net.E.put(net.nodes[sc.nextInt()], sc.nextInt());
		}
		net.targetVariable = net.nodes[sc.nextInt()];// 3
		net.Nd = sc.nextInt(); // 2
		net.usefulness = new double[net.targetVariable.K][net.Nd];
		for (int i = 0; i < net.targetVariable.K * net.Nd; ++i) {
			net.usefulness[sc.nextInt()][sc.nextInt()] = sc.nextDouble();
		}
		return net;
	}

	/**
	 * For debugging purposes.
	 */
	public static void printInput(DecisionNet net) {
		System.out.print(net.Nv + "\n");
		for (int i = 0; i < net.Nv; ++i) {
			System.out.print(net.nodes[i].K + "\t" + net.nodes[i].Npa + "\t");
			for (int j = 0; j < net.nodes[i].Npa; ++j) {
				System.out.print(net.nodes[i].parents[j].ID + "\t");
			}
			int j = 0;
			for (String combination : net.nodes[i].maps[0].keySet()) {
				if (net.nodes[i].combinations() > 1) {
					System.out.print(combination + ":");
				}
				for (int k = 0; k < net.nodes[i].K; ++k) {
					System.out.print(net.nodes[i].probability(k, combination));
					if (k < net.nodes[i].K - 1) {
						System.out.print(",");
					} else {
						if (j < net.nodes[i].combinations() - 1) {
							System.out.print("\t");
						} else {
							System.out.print("\n");
						}
					}
				}
				++j;
			}
		}
		System.out.print(net.Ne + "\n");
		for (Node node : net.E.keySet()) {
			System.out.print(node.ID + "\t" + net.E.get(node) + "\n");
		}
		System.out.print(net.targetVariable.ID + "\n");
		System.out.print(net.Nd + "\n");
		for (int i = 0; i < net.targetVariable.K; ++i) {
			for (int j = 0; j < net.Nd; ++j) {
				System.out.print(i + "\t" + j + "\t" + net.usefulness[i][j] + "\n");
			}
		}
	}

	public static void calculateAndPrintOutput(DecisionNet net) {
		double[] distribution = net.enumerationAsk();
		for (int i = 0; i < distribution.length; ++i) {
			System.out.print(distribution[i] + "\n");
		}
		double best_value = Double.NEGATIVE_INFINITY;
		int best_choice = -1;
		for (int i = 0; i < net.Nd; ++i) {
			double value = 0.0;
			for (int j = 0; j < net.targetVariable.K; ++j) {
				value += distribution[j] * net.usefulness[j][i];
			}
			if (value > best_value) {
				best_value = value;
				best_choice = i;
			}
		}
		System.out.print(best_choice + "\n");
	}

	public static void main(String[] args) {
		DecisionNet net = readInput();
		//printInput(net);
		calculateAndPrintOutput(net);
	}

}
